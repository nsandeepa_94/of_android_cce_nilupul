//
// Created by Nilupul Sandeepa on 2021-05-28.
//

#include "Camera.h"
#include "../../../../../libs/openFrameworks/math/ofVec3f.h"
#include "../../../../../libs/openFrameworks/graphics/ofGraphics.h"

NS_ADE_BEGIN

Camera::Camera() {
    this->zoomValue = 1;
    this->originStart = ofPoint(ofVec2f(0, 0));
    this->origin = ofPoint(ofVec2f(0, 0));
    this->minZoom = 1;
}

Camera::Camera(int width, int height) {
    this->width = width;
    this->height = height;
    this->zoomValue = 1;
    this->originStart = ofPoint(ofVec2f(0, 0));
    this->origin = ofPoint(ofVec2f(0, 0));
    this->minZoom = 1;
    this->isPanning = false;
    this->isZooming = false;
}

void Camera::setup(int width, int height) {
    this->width = width;
    this->height = height;
}

void Camera::begin() {
    ofPushMatrix();
    ofTranslate(this->origin.x, this->origin.y);
    ofScale(zoomValue, zoomValue);
}

void Camera::end() {
    ofPopMatrix();
}

void Camera::zoom(ade::Touch center, float scale, EventState state) {
    if (state == EventState::START) {
        this->isZooming = true;
        if (!this->isPanning) {
            this->resetOffsets();
        }
        this->zoomStart = zoomValue;
        this->zoomOnScreen = ofPoint(ofVec2f(center.x, center.y));
        this->zoomCenter = this->toCanvasPosition(ofPoint(ofVec2f(center.x, center.y)));
    } else if (state == EventState::END) {
        if (!this->isZooming) {
            return;
        }
        this->isZooming = false;
    }
    this->zoomValue = fminf(16.0, fmaxf(this->zoomStart * scale, this->minZoom));
    this->zoomOffset = (this->zoomStart - this->zoomValue) * this->zoomCenter;
    this->origin = this->originStart + this->panOffset + this->zoomOffset;
    this->restrictOrigin();
}

void Camera::pan(ade::Touch translate, EventState state) {
    if (state == EventState::START) {
        this->isPanning = true;
        if (!this->isZooming) {
            resetOffsets();
        }
    }
    this->panOffset = ofPoint(ofVec2f(translate.x, translate.y));
    if (state == EventState::END) {
        if (!this->isPanning) {
            return;
        }
        this->isPanning = false;
        this->restrictOrigin();
    }
}

float Camera::getZoom() {
    return this->zoomValue;
}

void Camera::setZoom(float zoom) {
    this->zoomValue = zoom;
}

void Camera::setMinimumZoom(float minZoom) {
    this->minZoom = minZoom;
}

ofPoint Camera::getCanvasTranslation() {
    float deltaZoom = (this->zoomValue - this->minZoom) / 2;
    return ofPoint(ofVec3f(this->origin.x + (this->width * deltaZoom), this->origin.y + (this->height * deltaZoom), this->zoomValue));
}

ofPoint Camera::toCanvasPosition(const ofPoint &screenPos) {
    return (screenPos - this->originStart - this->panOffset - this->zoomOffset) / this->zoomValue;
}

ofPoint Camera::toCanvasPosition(const ade::Touch &screenPos) {
    return (ofPoint(ofVec2f(screenPos.x, screenPos.y)) - this->origin) / this->zoomValue;
}

ofPoint Camera::toTouchPosition(const ofPoint &touchPos) {
    return (touchPos * this->zoomValue) + this->origin;
}

void Camera::restrictOrigin() {
    this->origin.x = fmaxf(fminf(this->origin.x, 0), (this->width * this->minZoom) - (this->width * this->zoomValue));
    this->origin.y = fmaxf(fminf(this->origin.y, 0), (this->height * this->minZoom) - (this->height * this->zoomValue));
}

void Camera::resetOffsets() {
    this->originStart = this->origin;
    this->panOffset.set(0, 0);
    this->zoomOffset.set(0, 0);
}

NS_ADE_END