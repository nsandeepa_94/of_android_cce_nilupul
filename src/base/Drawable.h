//
// Created by Nilupul Sandeepa on 2021-05-29.
//

#ifndef DRAWABLE_H
#define DRAWABLE_H

#include "../utils/Macros.h"
#include "../utils/Types.h"

NS_ADE_BEGIN

class Drawable {
    public:
        Drawable() {
            this->blendAlpha = 1;
            this->blendMode = BlendMode::NORMAL;
        }

        virtual void setup() = 0;
        virtual void update() = 0;
        virtual void draw() = 0;
        virtual void drawUI() = 0;

        float getBlendAlpha() {
            return this->blendAlpha;
        }

        virtual void setBlendAlpha(float alpha) {
            this->blendAlpha = alpha;
        }

        BlendMode getBlendMode() {
            return this->blendMode;
        }

        void setBlendMode(BlendMode mode) {
            this->blendMode = mode;
        }

    protected:
        float blendAlpha;
        BlendMode blendMode;
};

NS_ADE_END

#endif //DRAWABLE_H
